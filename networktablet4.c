#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/input.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <limits.h>
#include <arpa/inet.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <stdint.h>
#include "protocol.h"

int main (int argc, char *argv[]) {
  if (argc < 1) {
    printf("no device send");
    return -1;
  }
  struct input_event event, event_end;
  char buffer[50];
  int a = sprintf(buffer, "/dev/input/%s", argv[1]);
  int fd = open(buffer, O_RDWR);
  if (fd < 0) {
    printf("Errro open %s:%s\n", buffer, strerror(errno));
    return -1;
  }
  srand(time(NULL));
  memset(&event, 0, sizeof(event));
  memset(&event, 0, sizeof(event_end));
  gettimeofday(&event.time, NULL);
  event.type = EV_ABS;
  event.code = ABS_PRESSURE;
  
  gettimeofday(&event_end.time, NULL);
  event_end.type = EV_SYN;
  event_end.code = SYN_REPORT;
  event_end.value = 0;
  while(1){
    switch(rand() % 4) {
        case 0:
            event.value = ntohs(100);
            break;
        case 1:
            event.value = ntohs(80);
            break;
        case 2:
            event.value = ntohs(50);
            break;
        case 3:
            event.value = ntohs(10);
            break;
    }
    write(fd, &event, sizeof(event));// Move the mouse
    write(fd, &event_end, sizeof(event_end));// Show move
    usleep(800);
  }
  close(fd);
  return 0;
}
