#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <arpa/inet.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <arpa/inet.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <stdint.h>
#include "protocol.h"

#define die(str, args...) { \
    perror(str); \
    exit(EXIT_FAILURE); \
}


int udp_socket;


int prepare_socket()
{
    int s;
    struct sockaddr_in addr;

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        die("error: prepare_socket()");

    bzero(&addr, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(GFXTABLET_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) == -1)
        die("error: prepare_socket()");

    return s;
}


void send_event(int fd, int type, int code, int value)
{
    struct input_event event;
    memset(&event, 0, sizeof(event));
    gettimeofday(&event.time, NULL);
    event.type = type;
    event.code = code;
    event.value = value;
    write(fd, &event, sizeof(event));// Move the mouse
}

void quit(int signal) {
    close(udp_socket);
}


int main(void)
{
    struct event_packet ev_pkt;
    udp_socket = prepare_socket();

    printf("GfxTablet driver (protocol version %u) is ready and listening on 0.0.0.0:%u (UDP)\n"
        "Hint: Make sure that this port is not blocked by your firewall.\n", PROTOCOL_VERSION, GFXTABLET_PORT);

    signal(SIGINT, quit);
    signal(SIGTERM, quit);
    int fd = open("/dev/input/event1", O_RDWR);
    if (fd < 0) {
        printf("Errro open mouseevent:%s\n", strerror(errno));
        return 0;
    }

    int fd2 = open("/dev/input/mouse1", O_WRONLY | O_NONBLOCK);
    if (fd2 < 0) {
        printf("Errro open mouse:%s\n", strerror(errno));
        return 0;
    }

    if (ioctl(fd2, UI_SET_ABSBIT, ABS_PRESSURE) < 0)
        die("error: ioctl UI_SETEVBIT ABS_PRESSURE");
    
    while (recv(udp_socket, &ev_pkt, sizeof(ev_pkt), 0) >= 9) {        // every packet has at least 9 bytes
        printf("."); fflush(0);

        if (memcmp(ev_pkt.signature, "GfxTablet", 9) != 0) {
            fprintf(stderr, "\nGot unknown packet on port %i, ignoring\n", GFXTABLET_PORT);
            continue;
        }
        ev_pkt.version = ntohs(ev_pkt.version);
        if (ev_pkt.version != PROTOCOL_VERSION) {
            fprintf(stderr, "\nGfxTablet app speaks protocol version %i but driver speaks version %i, please update\n",
                ev_pkt.version, PROTOCOL_VERSION);
            break;
        }


/*        ev_pkt.x = ntohs(ev_pkt.x);*/
/*        ev_pkt.y = ntohs(ev_pkt.y);*/
        ev_pkt.pressure = ntohs(ev_pkt.pressure);
        printf("x: %hu, y: %hu, pressure: %hu\n", ev_pkt.x, ev_pkt.y, ev_pkt.pressure);

/*        send_event(device, EV_ABS, ABS_X, ev_pkt.x);*/
/*        send_event(device, EV_ABS, ABS_Y, ev_pkt.y);*/


        switch (ev_pkt.type) {
            case EVENT_TYPE_MOTION:
                send_event(fd, EV_ABS, ABS_PRESSURE, ev_pkt.pressure);
                send_event(fd, EV_SYN, SYN_REPORT, 1);
/*        send_event(device, EV_ABS, ABS_Y, ev_pkt.y);
                puts("gbsdfghhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
                break;
            case EVENT_TYPE_BUTTON:
/*                // stylus hovering*/
/*                if (ev_pkt.button == -1)*/
/*                    send_event(device, EV_KEY, BTN_TOOL_PEN, ev_pkt.down);*/
/*                // stylus touching*/
/*                if (ev_pkt.button == 0)*/
/*                    send_event(device, EV_KEY, BTN_TOUCH, ev_pkt.down);*/
/*                // button 1*/
/*                if (ev_pkt.button == 1)*/
/*                    send_event(device, EV_KEY, BTN_STYLUS, ev_pkt.down);*/
/*                // button 2*/
/*                if (ev_pkt.button == 2)*/
/*                    send_event(device, EV_KEY, BTN_STYLUS2, ev_pkt.down);*/
/*                // Left click*/
/*                if (ev_pkt.button == 3)*/
/*                    send_event(device, EV_KEY, BTN_LEFT, ev_pkt.down);*/
                break;
        }
/*        send_event(device, EV_SYN, SYN_REPORT, 1);*/
    }
    close(udp_socket);
    close(fd);
    close(fd2);
    printf("Removing network tablet from device list\n");

    printf("GfxTablet driver shut down gracefully\n");
    return 0;
}
