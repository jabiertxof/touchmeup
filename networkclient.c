#include <stdio.h>    //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h>    //inet_addr
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include "protocol.h"
#include <stdio.h>
#include <string.h>

#define die(str, args...) { \
    perror(str); \
    exit(EXIT_FAILURE); \
}

int udp_socket;

int prepare_socket(struct sockaddr_in addr)
{
    int s;
    s = socket(AF_INET , SOCK_DGRAM , 0);
    if (s == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    
    //Connect to remote server
    if (connect(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        printf("connect failed. Error");
        return -1;
    } else {
        puts("Connected\n");
    }
    return s;
}

int main(int argc , char *argv[])
{
    struct event_packet ev_pkt;
    struct sockaddr_in addr;
    int server_reply;
    bzero(&addr, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(GFXTABLET_PORT);
    addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    udp_socket = prepare_socket(addr);
    srand(time(NULL));
    //keep communicating with server
    int protocol = ntohs(2);

    
    FILE *inpipe;
    char inbuf[1000];
    int lineno = 0;
    char *command = "xinput --test 11";
    inpipe = popen(command, "r");
    if (!inpipe) {
        printf("couldn't open pipe %s\n", command);
        return 0;
    }

    int x = 0;
    int y = 0;
    while (fgets(inbuf, sizeof(inbuf), inpipe)) {
        int motion  = strstr(inbuf, "motion"   ) != 0 ? 1 : 0;
        int press   = strstr(inbuf, "press   1") != 0 ? 1 : 0;
        int release = strstr(inbuf, "release 1") != 0 ? 1 : 0;
        if (motion) {
/*            char *str = strdup(inbuf);*/
/*            char *token;*/
/*            while ((token = strsep(&str, " "))){*/
/*                char * buff = strdup(token); */
/*                int isx = strstr(token,"a[0]=") != 0 ? 1 : 0;*/
/*                int isy = strstr(token,"a[1]=") != 0 ? 1 : 0;*/

/*                if(isx) {*/
/*                    int strlen_x = strlen(token);*/
/*                    x = atoi(strncpy(token+5, buff+5 , strlen_x));*/
/*                    puts(strncpy(token+5, buff+5 , strlen_x));*/
/*                }*/
/*                if(isy) {*/
/*                    int strlen_y = strlen(token);*/
/*                    y = atoi(strncpy(token+5, buff+5 , strlen_y));*/
/*                    puts(strncpy(token+5, buff+5 , strlen_y));*/
/*                }            */
/*                */
/*                free(buff);*/
/*            }*/
/*            free(str);*/
/*            free(token);*/
            switch(rand() % 4) {
            case 0:
                ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_MOTION, {x, y, 100}, {3, 1}};
                break;
            case 1:
                ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_MOTION, {x, y, 19}, {3, 1}};
                break;
            case 2:
                ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_MOTION, {x, y, 9}, {3, 1}};
                break;
            case 3:
                ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_MOTION, {x, y, 59}, {3, 1}};
                break;
            }
            if( sendto(udp_socket,&ev_pkt,sizeof(struct event_packet),0, (struct sockaddr *)&addr, sizeof(ev_pkt)) < 0){
                puts("Send failed");
            }
/*        } else if (press) {*/

/*            ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_BUTTON, {x, y, 99}, {3, press}};//implicit release*/
/*            if( sendto(udp_socket,&ev_pkt,sizeof(struct event_packet),0, (struct sockaddr *)&addr, sizeof(ev_pkt)) < 0){*/
/*               puts("Send failed");*/
/*            } else {*/
/*                if (press) {*/
/*                    puts("start");*/
/*                } else {*/
/*                    puts("end");*/
/*                }*/
/*            };*/
/*            ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_BUTTON, {x, y, 99}, {3, 0}};//implicit release*/
/*            if( sendto(udp_socket,&ev_pkt,sizeof(struct event_packet),0, (struct sockaddr *)&addr, sizeof(ev_pkt)) < 0){*/
/*               puts("Send failed");*/
/*            } else {*/
/*                if (press) {*/
/*                    puts("start");*/
/*                } else {*/
/*                    puts("end");*/
/*                }*/
/*            };*/
        }
        usleep(800);
    }
/*    ev_pkt = (struct event_packet){"GfxTablet", protocol, EVENT_TYPE_BUTTON, {x, y, 99}, {3, 0}};//implicit release*/
/*    if( sendto(udp_socket,&ev_pkt,sizeof(struct event_packet),0, (struct sockaddr *)&addr, sizeof(ev_pkt)) < 0){*/
/*       puts("FINNNNNAAALLLLLL");*/
/*    }*/
    pclose(inpipe);
    close(udp_socket);
    return 0;
}
